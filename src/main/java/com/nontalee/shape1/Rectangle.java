/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nontalee.shape1;

/**
 *
 * @author nonta
 */
public class Rectangle extends Shape {

    private double widgth;
    private double height;

    public Rectangle(double widgth, double height) {
        super("Rectangle");
        this.widgth = widgth;
        this.height = height;
    }

    public double getWidgth() {
        return widgth;
    }

    public double getHeight() {
        return height;
    }

    public void setWidgth(double widgth) {
        this.widgth = widgth;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "Rectangle{" + "widgth=" + widgth + ", height=" + height + '}';
    }

    @Override
    public double calArea() {
        return widgth * height;
    }

}
